#ifndef __PLAYER_H__
#define __PLAYER_H__

#define DEPTH         (4)

#include <iostream>
#include <stdlib.h>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Board *board;
    Side my_side;
    Side other;
    int turns;
};

#endif
