#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);

    score = 0;
    lowest_score = 0;
    num_parents = 0;
    parent = NULL;
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}


/*
 * Returns nothing.  Creates a tree of future boards from a vector of valid moves.
 */

void Board::create_move_tree(vector<Board*> &move_tree, vector<Move> valid_moves, Board *board, Side side) {
    
    // Create a copy of the board for each legal move.
    for(unsigned int i = 0; i < valid_moves.size(); i++) {
        Board *copy = board->copy();
        move_tree.push_back(copy);
    }
    // Update each board for each valid move
    for(unsigned int j = 0; j < valid_moves.size(); j++) {
        move_tree[j]->doMove(&valid_moves[j], side);
        (move_tree[j]->last_move).x = valid_moves[j].x;
        (move_tree[j]->last_move).y = valid_moves[j].y;
    }
}

/*
 * Removes boards stored in a vector
 */

void Board::remove(vector<Board*> &boards)
{
    while(boards.size() > 0) {
        Board *board = boards.back();
        boards.pop_back();
        delete board;
    }
}

/*
 * Implementation of minimax algorithm.  Searches to arbitrary depth. Takes board as input.  
 * Outputs index of best move. 
 */

int Board::minimax(Board *board, int depth, Side my_side, Side other)
{
    Side current_side = (board->num_parents % 2 == 0) ? my_side : other;
    // More or less the base case.  Last level of the tree.
    if (board->num_parents >= depth-1){
        // Find legal moves
        std::vector<Move> valid_moves;
        board->Board::whichMoves(current_side, valid_moves);
    
        // There are no legal moves
        if (valid_moves.size() < 1) {
            board->lowest_score = board->calculate_score(board, my_side, other); 
            /*if (current_side == my_side){
                return VERY_BAD;
            }
            else if (current_side == other){
                return VERY_GOOD;
            } 
            */ 
            return board->lowest_score;
        }

        // There are legal moves
        board->create_move_tree(board->next, valid_moves, board, current_side);
        
        // Set board to be parent
        for(unsigned int i = 0; i < (board->next).size(); i++) {
            ((board->next)[i])->parent = board;
            ((board->next)[i])->num_parents = board->num_parents + 1;
        }

        // Calculate each board's score
        for(unsigned int j = 0; j < (board->next).size(); j++) {
            ((board->next)[j])->score = board->calculate_score((board->next)[j], my_side, other); 
        }

        int mmx = 0;
        // Determine what the maximum score is
        if (current_side == my_side) 
            mmx = board->determine_best(board->next, VAL);
        

        // Determine what the minimum score is
        if (current_side == other) 
            mmx = board->determine_worst(board->next, VAL);
        

        board->remove(board->next);
        //board->lowest_score = worst;
        return mmx;
    }

    // At the top of the tree.  Involves recursion.  Return index of best move instead of 
    //    value of worst move, as in the other cases.
    else if (board->num_parents == 0){
        // Find legal moves
        std::vector<Move> valid_moves;
        board->Board::whichMoves(current_side, valid_moves);

        // There are guaranteed to be legal moves at this point.
        board->create_move_tree(board->next, valid_moves, board, current_side);
        
        // Set board to be parent
        for(unsigned int i = 0; i < (board->next).size(); i++) {
            ((board->next)[i])->parent = board;
            ((board->next)[i])->num_parents = board->num_parents + 1;
        }
        
        // Recursive part. Run minimax on each possible next move.
        vector<int> worst_scores;
        for(unsigned int j = 0; j < (board->next).size(); j++) {
            worst_scores.push_back( board->minimax((board->next)[j], depth, my_side, other) );
        }
        int mmx = maximum(worst_scores, LOCATION);
        board->remove(board->next);
        return mmx;
    }

    // At some other depth in the tree.  Not the top and not the bottom.  This involves recursion.
    else if (board->num_parents < depth-1){
        // Find legal moves
        std::vector<Move> valid_moves;
        board->Board::whichMoves(current_side, valid_moves);
    
        // There are no legal moves
        if (valid_moves.size() < 1) {
            /*if (current_side == my_side){
                return VERY_BAD;
            }
            else if (current_side == other){
                return VERY_GOOD;
            } 
            */ 
            board->lowest_score = board->calculate_score(board, my_side, other);   
            return board->lowest_score;
        }

        // There are legal moves
        board->create_move_tree(board->next, valid_moves, board, current_side);
        
        // Set board to be parent
        for(unsigned int i = 0; i < (board->next).size(); i++) {
            ((board->next)[i])->parent = board;
            ((board->next)[i])->num_parents = board->num_parents + 1;
        }
        
        // Recursive part. Run minimax on each possible next move.
        vector<int> worst_scores;
        for(unsigned int j = 0; j < (board->next).size(); j++) {
            worst_scores.push_back( board->minimax((board->next)[j], depth, my_side, other) );
        }
   
        int mmx = 0;
        if (current_side == my_side)
            mmx = maximum(worst_scores, VAL);
        
        if (current_side == other)
            mmx = minimum(worst_scores, VAL);

        board->remove(board->next);
        return mmx;
    }
    else {
        return 0;
    }
}


/*
 * Calculate the score of a particular board using a heuristic function.
 */

int Board::calculate_score(Board *board, Side my_side, Side other) {
    
    // Calculate score based on number of my pieces at the end of a turn
    int my_pieces_score = count(my_side) * PIECES;

    // Calculate score based on number of moves available to opponent
    std::vector<Move> their_moves;
    board->whichMoves(other, their_moves);
    int their_move_score = their_moves.size() * THEIR_MOVES;

    // Calculate score based on number of moves availabe to me
    std::vector<Move> my_moves;
    board->whichMoves(my_side, my_moves);
    int my_move_score = my_moves.size() * MY_MOVES;

    // Calculate score based on number of spaces adjacent to my pieces
    int my_adj_score = count_adjacent(board, my_side) * MY_ADJ;    

    // Calculate score based on number of spaces adjacent to their pieces
    int their_adj_score = count_adjacent(board, other) * THEIR_ADJ;

    // Calculate score based on position of the last move
    int last_move_score = location_score(board->last_move);
    
    int total_score = my_pieces_score + their_move_score + my_move_score + my_adj_score + their_adj_score + last_move_score;
    
    return total_score;
}

/*
 * Determine the score associated with a particular location on the board.
 * An unordered map might be a nice substitute, but there might be an issue with Move operators.
 */

int Board::location_score(Move move) {
    int X = move.x;
    int Y = move.y;

    // (0,0), (0,7), (7,0), and (7,7) are considered corner locations
    if ( (X==0 && Y==0) || (X==0 && Y==7) || (X==7 && Y==0) || (X==7 && Y==7) )
        return CORNER;

    // (0,2->5), (7,2->5), (2->5,0), and (2->5,7) are good edge locations
    else if ( (X==0 && (Y>=2 && Y<=5)) || (X==7 && (Y>=2 && Y<=5)) || 
              ((X>=2 && X<=5) && Y==0) || ((X>=2 && X <=5) && Y==7) ) {
        return EDGE;
    }
    
    // (0,1), (1,0), (6,0), (7,1), (0,6), (1,7), (7,6), and (6,7) are bad edge locations
    else if ( (X==0 && Y==1) || (X==1 && Y==0) || (X==6 && Y==0) || (X==0 && Y==6) ||
              (X==7 && Y==1) || (X==1 && Y==7) || (X==7 && Y==6) || (X==6 && Y==7) ) {
        return BAD_EDGE;
    }

    // (1,1), (6,1), (1,6), (6,6) are bad corner locations
    else if ( (X==1 && Y==1) || (X==6 && Y==1) || (X==1 && Y==6) || (X==6 && Y==6) )
        return BAD_CORNER;

    else
        return 0;
}

/*
 * Finds the maximum of a vector of values and returns its location or returns the actual value
 */

int Board::maximum(vector<int> values, bool location) {
    int max = values[0];
    int index = 0;
    for(unsigned int i = 1; i < values.size(); i++) {
        if (values[i] > max){
            max = values[i];
            index = i;
        }
    }
    if(location)
        return index;

    return max;
}

/*
 * Finds the minimum of a vector of values and returns its location or returns the actual value
 */

int Board::minimum(vector<int> values, bool location) {
    int min = values[0];
    int index = 0;
    for(unsigned int i = 1; i < values.size(); i++) {
        if (values[i] < min){
            min = values[i];
            index = i;
        }
    }
    if(location)
        return index;

    return min;
}

/*
 * Calculates the number of spaces adjacent to all of a side's pieces
 */

int Board::count_adjacent(Board* board, Side side) {
    int adj_count = 0;
    // Step through entire board, except the edges - no need to check them
    for(int i = 1; i < 7; i++) {
        for(int j = 1; j < 7; j++) {
            // If you find one of your pieces, check if the adjacent spaces are empty
            if ( get(side, i, j) ) {
                for(int x = i-1; x <= i+1; x++) {
                    for(int y = j-1; y <= j+1; y++) {
                        // Count the number of unoccupied adjacent spaces
                        if( x != i && y != j && !occupied(x, y) ) {
                            adj_count++;
                        }
                    }
                }
            }
        }
    }
    return adj_count;
}
 
/*
 * Determines the best move from a list of moves based on their individual scores.
 * Takes a vector of boards as input.  Returns the index of the board with the best move. 
 */

int Board::determine_best(vector<Board*> &move_tree, bool location) {
    std::vector<int> scores;
    for(unsigned int i = 0; i < move_tree.size(); i++) {
        scores.push_back( move_tree[i]->score );
    }
    int index = maximum(scores, location);
     
    return index;
}

/*
 * Determines the worst move from a list of moves based on their individual scores.
 * Takes a vector of boards as input.  Returns the score of the board with the best move. 
 */

int Board::determine_worst(vector<Board*> &move_tree, bool location) {
    std::vector<int> scores;
    for(unsigned int i = 0; i < move_tree.size(); i++) {
        scores.push_back( move_tree[i]->score );
    }
    int min = minimum(scores, location);
     
    return min;
}

/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns nothing.  Pass a vector by reference and push valid moves onto it.
 */

void Board::whichMoves(Side side, vector<Move> &moves) {
    for(int i = 0; i < 8; i++) {
        for(int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side))
                moves.push_back(move);
        }
    }
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
