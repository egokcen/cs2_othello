#include "player.h"

/*
 * LOOK I MADE A CHANGE
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    
    // Initialize board
    board = new Board();
    // Initialize player side
    my_side = side;
    other = (my_side == BLACK) ? WHITE : BLACK;
    turns = 0;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	delete board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
/////////
string mystr;
getline(cin, mystr);
/////////      
    // First, process opponent's move - assume that opponent's move is valid

    // Update board
    if (opponentsMove != NULL) {
    	std::cerr << "Opp Move to: " << opponentsMove->x << " : " << opponentsMove->y << std::endl;
        board->Board::doMove(opponentsMove, other);
    } 
    std::cerr << "Opponent's Move Updated" << std::endl;

    // Find legal moves
    std::vector<Move> valid_moves;
    board->Board::whichMoves(my_side, valid_moves);
    
    // There are no legal moves
    if (valid_moves.size() < 1) {
        std::cerr << "No legal moves found." << std::endl;   
        return NULL;
    }

    // There are legal moves
    std::cerr << "Found legal moves: " << valid_moves.size() << std::endl;

    // Pick a move using a heuristic function

    // Minimax!
    int best = board->Board::minimax(board, DEPTH, my_side, other);


    //std::vector<Board*> move_tree;
    //board->Board::create_move_tree(move_tree, valid_moves, board, my_side);

    // Calculate each board's score
    //for(unsigned int i = 0; i < move_tree.size(); i++) {
        //move_tree[i]->score = board->Board::calculate_score(move_tree[i], my_side, other);
    //}

    // Determine which board has the highest score (that board is associated with a move)
    //int best = board->Board::determine_best(move_tree);

    // Update the board with our chosen move
    std::cerr << "Using move: " << best << std::endl;
    board->Board::doMove(&valid_moves[best], my_side);

    // Clear out the simulated boards
    //Board::clear_tree(move_tree);

//    Move current_move = valid_moves[random_move];
//    board->Board::doMove(&valid_moves[random_move], my_side);
	
	Move *my_move = new Move(-1, -1);
    my_move->x = valid_moves[best].x;    
    my_move->y = valid_moves[best].y;        

    turns++;
    std::cerr << "Number of moves made: " << turns << std::endl << std::endl;   // For debugging
    //std::cerr << "Move to: " << my_move->x << " : " << my_move->y << std::endl;
    //Move *m = new Move(1, 1);
    //if (testingMinimax == true) return m;
    return my_move;
}
