#ifndef __BOARD_H__
#define __BOARD_H__

#define PIECES        (4)
#define MY_MOVES      (1)
#define THEIR_MOVES   (-1)
#define MY_ADJ        (-1)
#define THEIR_ADJ     (1)
#define CORNER        (10)
#define EDGE          (5)
#define BAD_EDGE      (-5)
#define BAD_CORNER    (-10)

#define VERY_BAD      (-100)
#define VERY_GOOD     (30)

#define LOCATION      (true)
#define VAL           (false)

#include <bitset>
#include <vector>
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
    
    void create_move_tree(vector<Board*> &move_tree, vector<Move> valid_moves, Board *board, Side side);
    void remove(vector<Board*> &boards);
    int minimax(Board* board, int depth, Side my_side, Side other);
   // void create_deep_move_tree(vector<Board*> &move_tree, vector<Move> valid_moves, Board *board, Side side);
    int calculate_score(Board *board, Side my_side, Side other);
    int location_score(Move move);
    int maximum(vector<int> scores, bool location);
    int minimum(vector<int> scores, bool location);
    int count_adjacent(Board* board, Side side);
    int determine_best(vector<Board*> &move_tree, bool location); 
    int determine_worst(vector<Board*> &move_tree, bool location);   
    bool isDone();
    bool hasMoves(Side side);
    //vector<Move> whichMoves(Side side);
    void whichMoves(Side side, vector<Move> &moves);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();

    void setBoard(char data[]);

    Move last_move;
    int score;
    int lowest_score;
    int num_parents;
    vector<Board*> next;
    Board *parent;
};

#endif
